--Ex1
--1
CREATE OR REPLACE view Produit10 as 
SELECT * FROM PRODUIT WHERE PUProd >= 10;

--2
CREATE OR REPLACE VIEW DixMars2015 AS 
SELECT distinct refProd, nomProd, PUProd, dateFac
FROM PRODUIT NATURAL JOIN FACTURE NATURAL JOIN DETAIL
WHERE dateFac = STR_TO_DATE('10,03,2015', '%d,%m,%Y');

--3
CREATE OR REPLACE VIEW NbClientsParVille AS 
SELECT adresseCli ville, count(numCli) FROM CLIENT
group by adresseCli;

select * from NbClientsParVille WHERE ville LIKE 'M%';

--4
CREATE OR REPLACE VIEW CAParMois AS
SELECT MONTH(dateFac), YEAR(dateFac) annee, sum(PUProd * qte) 
FROM PRODUIT NATURAL JOIN DETAIL NATURAL JOIN FACTURE
group by MONTH(dateFac), YEAR(dateFac);

--5
DROP VIEW CAParMois;
DROP VIEW NbClientsParVille ;
DROP VIEW  DixMars2015;
DROP VIEW  Produit10;

--eX2
--1
SELECT distinct nomProd
FROM DETAIL NATURAL RIGHT JOIN  PRODUIT
where qte is null OR numFac is null;

--2
select nomProd, count(numFac)
FROM PRODUIT NATURAL LEFT JOIN DETAIL
where nomProd LIKE 'M%'
group by nomProd;

--3
select nomProd, sum(IFNULL(qte,0)) quantiteeTotale
FROM PRODUIT NATURAL LEFT JOIN DETAIL
where nomProd LIKE 'M%'
group by nomProd;


--EX3
--1
CREATE OR REPLACE VIEW Annee AS 
SELECT distinct YEAR(dateFac) annee 
FROM FACTURE NATURAL JOIN DETAIL;

CREATE OR REPLACE VIEW ClientAnnee AS
SELECT *
FROM CLIENT NATURAL JOIN Annee;

CREATE OR REPLACE VIEW CAParClientParAn AS
SELECT numCli, YEAR(dateFac) annee, sum(qte * PUProd) CA
FROM  FACTURE NATURAL JOIN  DETAIL NATURAL JOIN PRODUIT
GROUP BY YEAR(dateFac),numCli;

select * from CAParClientParAn NATURAL RIGHT JOIN ClientAnnee where nomCli= 'ZET';

--2
CREATE OR REPLACE view ProdAnnee AS
SELECT  DISTINCT refProd, nomProd, PUProd, YEAR(dateFac) annee
FROM PRODUIT NATURAL JOIN DETAIL NATURAL JOIN FACTURE
GROUP BY YEAR(dateFac), refProd;

SELECT * FROM ProdAnnee;

