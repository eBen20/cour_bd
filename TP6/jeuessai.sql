INSERT INTO AUTEUR VALUES
    ("1", "Zola", "Emile"),
    ("2", "Uderzo", "Albert"),
    ("3", "Molière", null);
INSERT INTO GENRE VALUES
    ("1", "roman"),
    ("2", "BD"),
    ("3", "pièce de théatre");

INSERT INTO LIVRE VALUES
    ("1", "Germinal", 1),
    ("2", "Asterix chez les bretons", "2"),
    ("3", "Le bourgeois gentilhomme", "3");



INSERT INTO ECRIRE VALUES
    ("1", "1"),
    ("2", "2"),
    ("3", "3");