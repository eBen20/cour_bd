-- TP 1
-- Nom:  , Prenom: 

-- +------------------+--
-- * Question 1 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  Donner la liste des panels dont fait partie Caroline BOURIER.

--select nomPan from PANEL natural join CONSTITUER natural join SONDE where nomSond='BOURIER' and prenomSond='Caroline';
--select distinct nomPan from PANEL NATURAL JOIN CONSTITUER where idPan in( select idPan from CONSTITUER natural join SONDE where nomSond='BOURIER' and prenomSond='Caroline');
select * from PANEL p2 natural join CONSTITUER natural join SONDE s2 where s2.nomSond='BOURIER' and s2.prenomSond='Caroline';

select p1.nomPan from PANEL p1 where exists (select * from CONSTITUER c natural join SONDE where nomSond='BOURIER' and prenomSond='Caroline' and c.idPan = p1.idPan);

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +-----------------+
-- | nomPan          |
-- +-----------------+
-- | France global 1 |
-- +-----------------+
-- = Reponse question 1.



-- +------------------+--
-- * Question 2 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  Quels sont les panels dont un des sondés est de la tranche d'âge 70 à 120 ans?

--select distinct nomPan from PANEL natural join CONSTITUER natural join SONDE where YEAR(NOW())-YEAR(dateNaisSond)>=70 and YEAR(NOW())-YEAR(dateNaisSond)<=120;


-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +-----------------+
-- | nomPan          |
-- +-----------------+
-- | France global 1 |
-- | France global 2 |
-- +-----------------+
-- = Reponse question 2.



-- +------------------+--
-- * Question 3 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  Quels sont les sondés de la tranche d'age 70-120 ans qui sont ouvriers?

--select nomSond, prenomSond from SONDE natural join CARACTERISTIQUE natural join CATEGORIE where intitulecat='Ouvriers' and YEAR(NOW())-YEAR(dateNaisSond)>=70 and YEAR(NOW())-YEAR(dateNaisSond)<=120 ;


-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +-----------+------------+
-- | nomSond   | prenomSond |
-- +-----------+------------+
-- | ERYS      | Imane      |
-- | BERRGAIES | Claire     |
-- | JABAT     | Rose       |
-- | WALLOCHE  | Marion     |
-- | LENUJA    | Pauline    |
-- | etc...
-- = Reponse question 3.



-- +------------------+--
-- * Question 4 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  Quels sont les ouvriers qui portent le prénom Olivier?

--select nomSond, prenomSond from SONDE natural join CARACTERISTIQUE natural join CATEGORIE where intitulecat='Ouvriers' and prenomSond='Olivier'

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +-----------+------------+
-- | nomSond   | prenomSond |
-- +-----------+------------+
-- | THALOUERD | Olivier    |
-- | POTRININ  | Olivier    |
-- +-----------+------------+
-- = Reponse question 4.



-- +------------------+--
-- * Question 5 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  Quels sont les tranches d'âge qui comportent une ou plusieurs femmes nées un 25 avril?

--select distinct valDebut, valFin from SONDE NATURAL JOIN CARACTERISTIQUE NATURAL JOIN TRANCHE WHERE DAY(dateNaisSond) = 25 and MONTH(dateNaisSond) = 04 and sexe = 'F' ;

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +----------+--------+
-- | valDebut | valFin |
-- +----------+--------+
-- | 40       | 49     |
-- +----------+--------+
-- = Reponse question 5.



-- +------------------+--
-- * Question 6 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  Quels sont les sondés prénommés Jean qui appartiennent à au moins 2 panels différents? 

--select distinct prenomSond, nomSond from CONSTITUER C1 NATURAL JOIN SONDE where prenomSond = "Jean" and numSond in(select numSond from CONSTITUER C2 where C1.idPan <> C2.idPan);
--select s1.prenomSond, s1.nomSond from Sonde s1 NATURAL JOIN CONSTITUER c1 where exist(select * from CONSTITUER c2  where c1.idPan <> c2.idPan and s1.numSond = c2.numSond);  

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +------------+----------+
-- | prenomSond | nomSond  |
-- +------------+----------+
-- | Jean       | DILY     |
-- | Jean       | JATECHU  |
-- | Jean       | PIETIENE |
-- | Jean       | FAL      |
-- | Jean       | BOYEGHE  |
-- +------------+----------+
-- = Reponse question 6.



