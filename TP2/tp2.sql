-- TP 2
-- GUERRE:  , Benjamon: 

-- +------------------+--
-- * Question 1 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  Quels sont les panels dont ne fait pas partie Louane DJARA?
select nomPan 
from PANEL 
where idPan not in( 
    select idPan 
    from CONSTITUER natural join SONDE 
    where nomSond = 'DJARA' and prenomSond = 'Louane'
);
-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +-----------------+
-- | nomPan          |
-- +-----------------+
-- | France global 1 |
-- | Moins de 50 ans |
-- +-----------------+
-- = Reponse question 1.



-- +------------------+--
-- * Question 2 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  Quels sont les prénoms de sondé commençant par un A qui n'apparaissent pas dans la tranche d'age 20-29 ans? Classez ces noms par ordre alphabétique.

select  distinct prenomSond
from SONDE
where 
    prenomSond LIKE 'A%' and prenomSond 
    not in
        ( select prenomSond 
        from SONDE natural join CARACTERISTIQUE natural join TRANCHE 
        where 
            idTR = 1)
ORDER BY prenomSond;  

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +------------+
-- | prenomSond |
-- +------------+
-- | Alice      |
-- | Allan      |
-- | Amaury     |
-- | Ambre      |
-- | Anaïs      |
-- | etc...
-- = Reponse question 2.



-- +------------------+--
-- * Question 3 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--   Quels sont les panels dont tous les sondés ont moins de 60 ans? Rappel: CURDATE() donne la date du jour et DATEDIFF(d1,d2) donne le nombre de jours entre d1 et d2.

select distinct nomPan from PANEL natural join CONSTITUER where nomPan not in 
    ( select nomPan from PANEL natural join CARACTERISTIQUE natural join SONDE 
        where DATEDIFF(CURDATE(), dateNaisSond) >= 60 * 365);

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +-----------------+
-- | nomPan          |
-- +-----------------+
-- | Moins de 50 ans |
-- +-----------------+
-- = Reponse question 3.



-- +------------------+--
-- * Question 4 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  Quelles sont les catégories qui comportent des personnes nées en 1976? On rappelle que YEAR(d) donne l'année de la date d sous la forme d'un entier.

select distinct intituleCat from CATEGORIE natural join CARACTERISTIQUE natural join SONDE where YEAR(dateNaisSond) = '1976'; 

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +-------------------------------------------------+
-- | intituleCat                                     |
-- +-------------------------------------------------+
-- | Cadres, professions intellectuelles supérieures |
-- | Professions intermédiaires                      |
-- | Employés                                        |
-- | Ouvriers                                        |
-- | Inactifs ayant déjà travaillé                   |
-- +-------------------------------------------------+
-- = Reponse question 4.



-- +------------------+--
-- * Question 5 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--   Quels sont les sondés nés en 1998 qui appartiennent aux panels France global 1 et France global 2?

select nomSond, prenomSond from SONDE where YEAR(dateNaisSond) = '1998' and numSond in
    ( select numSond from CONSTITUER natural join PANEL where nomPan = "France global 1" and numSond in 
        ( select numSond from CONSTITUER natural join PANEL where nomPan = "France global 2"));

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +------------+------------+
-- | nomSond    | prenomSond |
-- +------------+------------+
-- | TRISAULOLU | Elise      |
-- | MAMIAT     | Mathieu    |
-- | NEUSIL     | Theo       |
-- +------------+------------+
-- = Reponse question 5.



-- +------------------+--
-- * Question 6 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  Quels sont les sondés nés en 1976 qui ont la même date de naissance?

select s1.* from SONDE s1, SONDE s2 where YEAR(s1.dateNaisSond) = 1976 and s2.dateNaisSond = s1.dateNaisSond and s1.numSond > s2.numSond;

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +---------+------------+----------+------------+
-- | nomSond | prenomSond | nomSond  | prenomSond |
-- +---------+------------+----------+------------+
-- | DASA    | Maxime     | PEKARDAC | Bilal      |
-- +---------+------------+----------+------------+
-- = Reponse question 6.



