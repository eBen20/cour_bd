DROP TABLE IF EXISTS EFFECTUER;
DROP TABLE IF EXISTS COURSE;
DROP TABLE IF EXISTS COUREUR;
DROP TABLE IF EXISTS CLUB;
DROP TABLE IF EXISTS EVENEMENT;
DROP TABLE IF EXISTS CATEGORIE;
DROP TABLE IF EXISTS LIEU;







CREATE TABLE EVENEMENT (
    idEv int,
    nomEv varchar(14),
    dateEv date,
    idL int,
    PRIMARY KEY (idEv)
);

CREATE TABLE LIEU (
    idL int,
    nomL varchar(14),
    PRIMARY KEY (idL)
);

CREATE TABLE COURSE (
    idCourse int,
    heure TIME,
    idEv int,
    idCat int,
    PRIMARY KEY (idEv, idCourse)
);

CREATE TABLE EFFECTUER (
    idCo int,
    idEv int,
    idCourse int, 
    temps time,
    PRIMARY KEY (idCo, idEv, idCourse)
);

CREATE TABLE COUREUR (
    idCo int,
    nomCo varchar(14),
    prenomCo varchar(14),
    idCl int,
    idCat int, 
    PRIMARY KEY (idCo)
);
CREATE TABLE CLUB (
    idCl int,
    signeCl varchar(14),
    nomCl varchar (14),
    PRIMARY KEY (idCl)
);

CREATE TABLE CATEGORIE (
    idCat int,
    nomCat varchar(14),
    PRIMARY KEY (idCat)
);

ALTER TABLE EVENEMENT ADD FOREIGN KEY (idL) REFERENCES LIEU (idL);
ALTER TABLE COURSE ADD FOREIGN KEY (idEv) REFERENCES EVENEMENT (idEv);
ALTER TABLE COURSE ADD FOREIGN KEY (idCat)  REFERENCES CATEGORIE (idCat);
ALTER TABLE EFFECTUER ADD FOREIGN KEY (idCo) REFERENCES COUREUR (idco);
ALTER TABLE EFFECTUER ADD FOREIGN KEY (idEv, idCourse) REFERENCES COURSE (idEv, idCourse);
ALTER TABLE COUREUR ADD FOREIGN KEY  (idCl) REFERENCES CLUB (idCl);
ALTER TABLE COUREUR ADD FOREIGN KEY (idCat) REFERENCES CATEGORIE (idCat);