--1
select * 
from EVENEMENT 
where idL = (select idl from LIEU WHERE nomL = "Orléans") 
order by dateEv;

--2
select distinct idCo, nomCo, prenomCo
from COUREUR NATURAL JOIN EFFECTUER 
where idCo in (select idCo 
    from COUREUR NATURAL JOIN EFFECTUER NATURAL JOIN COURSE NATURAL JOIN EVENEMENT
    WHERE idCl = 1 and idEv = 1 and idCat = 3)
order by temps;

--3
select idCourse, heure 
from COURSE NATURAL JOIN EFFECTUER NATURAL JOIN COUREUR
where idCl = 1;

--4
select idCo, nomCo, prenomCo
from COUREUR NATURAL JOIN EFFECTUER
WHERE idCat = 1
having count(idCo) >= 2 ;

--5
select  distinct idCo, nomCo, prenomCo
from COUREUR NATURAL JOIN EFFECTUER NATURAL JOIN COURSE
WHERE idCo in (select idCo from COURSE natural join EFFECTUER NATURAL JOIN EVENEMENT where idEv = 3 and idL = 1 ) and idCo in (select idCo from COURSE natural join EFFECTUER NATURAL JOIN EVENEMENT where idEv = 2 and idL = 2);

--6
CREATE OR REPLACE VIEW nbCourseCat as select idCat, count(idCourse) nbCourse from COURSE group by idCat;
select idCo, nomCo, prenomCo, count(idCourse) nb, nbCourse
from COUREUR natural join EFFECTUER NATURAL JOIN COURSE NATURAL JOIN nbCourseCat
group by idCo, idCat
having nb = nbCourse;

--7
select idCourse, sum(temps)/count(idco) tempsMoyen
from COURSE NATURAL JOIN EFFECTUER
group by idCourse, idCat;
 
--8
select idEv, count(idCo) nbDeCoureurs
from COURSE NATURAL JOIN EFFECTUER
group by idEv;