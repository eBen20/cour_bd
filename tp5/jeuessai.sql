source creation.sql;

-- SET FOREIGN_KEY_CHECKS=0;

insert into LIEU  values
(1, "Orléans"),
(2, "OLIVET"),
(3, "Tours")
;

insert into CLUB values
(1, "USO", "Union sportive d'orléans"),
(2, "ASM", "Association Sportive de Montagi")
;

insert into EVENEMENT values
(1, "Foulées d’Orléans", STR_TO_DATE('12/12/2021','%d/%m/%Y'), 1 ),
(2, "Corrida d'Olivet", STR_TO_DATE('25/12/2021','%d/%m/%Y'), 2),
(3, "Cross'O", STR_TO_DATE('10/01/2016','%d/%m/%Y'), 1),
(4, "Vite-Tours", STR_TO_DATE('18/03/2022','%d/%m/%Y'), 3)
;

insert into CATEGORIE values
(1, "junior"),
(3, "senior")
;

INSERT INTO COUREUR values
(1, "DUPOND", "Jean", 1, 3),
(2, "DUVAL", "Marie", 2, 1),
(3, "BENZ", "Karim", 1, 3)
;

insert into COURSE  values
(1, STR_TO_DATE('09', '%h'), 1, 3),
(2, STR_TO_DATE('11', '%h'), 2, 1),
(3, STR_TO_DATE('10', '%h'), 3, 1),
(4, STR_TO_DATE('09', '%h'), 4, 3),
(5, STR_TO_DATE('0930', '%h%i'), 4, 1)
;

insert into EFFECTUER  values
(1, 1, 1, STR_TO_DATE('1232', '%i%s')),
(2, 2, 2, STR_TO_DATE('1153', '%i%s')),
(2, 3, 3, STR_TO_DATE('0859', '%i%s')),
(3 ,1, 1, STR_TO_DATE('0933', '%i%s')),
(3 ,4, 4, STR_TO_DATE('2032', '%i%s'))
;

-- SET FOREIGN_KEY_CHECKS=1;


