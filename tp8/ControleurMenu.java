import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.MenuItem;

import java.sql.SQLException;
import java.util.ArrayList;

public class ControleurMenu implements EventHandler<ActionEvent> {
    TestJDBC testJDBC;
    ControleurMenu(TestJDBC testJDBC){
        this.testJDBC=testJDBC;
    }
    @Override
    public void handle(ActionEvent actionEvent) {
        FicheEmploye ficheEmploye=this.testJDBC.getFicheEmploye();
        if (actionEvent.getTarget().getClass().equals(MenuItem.class)){
            String etiquette=((MenuItem)actionEvent.getTarget()).getText();
            switch (etiquette){
                case "Connexion":
                    this.testJDBC.showFenetreConnexion();
                    break;
                case "Déconnexion":
                    try{
                    this.testJDBC.getConnexionMySQL().close();
                    } catch(Exception e){}
                    this.testJDBC.deconnexionReussie();
                    break;
                case "Quitter":
                    try {
                        this.testJDBC.getConnexionMySQL().close();
                        this.testJDBC.stop();
                    } catch (Exception e) { }
                    Platform.exit();
                    System.exit(0);
                    break;

                case "Ajouter un employe":
                    ficheEmploye.setNomBouton("Créer");
                    ficheEmploye.viderFiche();
                    this.testJDBC.showFicheEmploye();
                    break;
		case "Salaires moyens":
                    String rapport="";
                    try {
                        rapport=this.testJDBC.getExerciceJDBC().salaireMoyenParService();
                    }catch (SQLException ex){
                        rapport="Le traitement a échoué\nVoici le message du serveur\n"+ex.getMessage();
                    }
                    this.testJDBC.showFicheResultat(rapport);
                    break;
        case "Annuaire":
                    String rapport2="";
                    try {
                        rapport2=this.testJDBC.getExerciceJDBC().annuaire();
                    }catch (SQLException ex){
                        rapport2="Le traitement a échoué\nVoici le message du serveur\n"+ex.getMessage();
                    }
                    this.testJDBC.showFicheResultat(rapport2);
                    break;
                default:
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
			        alert.setTitle("Option non connue !!!! ");
			        alert.setHeaderText("Option "+etiquette+" inconnue");
			        alert.setContentText("Problème de définition des menus");
			        alert.showAndWait();
            }
        }
    }
}
