CREATE DATABASE IF NOT EXISTS `RH` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `RH`;
DROP table CONTRAT;
DROP TABLE EMPLOYE;
Drop TABLE SERVICE;

CREATE TABLE `SERVICE` (
  `nums` int,
  `noms` varchar(30),
  PRIMARY KEY (`nums`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `EMPLOYE` (
  `nume` int,
  `nome` varchar(30),
  `prenome` varchar(30),
  `telephone` decimal(10),
  `nums` int,
  PRIMARY KEY (`nume`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `CONTRAT` (
  `numc` int,
  `datedebut` date,
  `datefin` date,
  `salaire` decimal(8,2),
  `nume` int,
  PRIMARY KEY (`numc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `EMPLOYE` ADD FOREIGN KEY (`nums`) REFERENCES `SERVICE` (`nums`);
ALTER TABLE `CONTRAT` ADD FOREIGN KEY (`nume`) REFERENCES `EMPLOYE` (`nume`);


drop role if EXISTS adm, direction, employe;
create role  if not exists adm, direction, employe;
GRANT ALL  on RH.* to adm;
CREATE user if not EXISTS bob;
GRANT adm to bob;
GRANT SELECT, UPDATE on RH.* to direction;
CREATE user if not EXISTS jean;
GRANT direction to jean;
GRANT SELECT on RH.SERVICE to employe;
GRANT SELECT on RH.EMPLOYE to employe;
CREATE user if not EXISTS toto;
GRANT employe to toto;