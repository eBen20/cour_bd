import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;

import java.sql.SQLException;

public class ControleurBouton implements EventHandler<ActionEvent> {
    private TestJDBC testJDBC;

    public ControleurBouton(TestJDBC testJDBC) {
        this.testJDBC=testJDBC;
    }

    private void alertOK( String message){
        Alert alert=new Alert(Alert.AlertType.INFORMATION);
        alert.setResizable(true);
        alert.setWidth(500);
        alert.setTitle("Bravo !!!! ");
        alert.setHeaderText("Opération réussie");
        alert.setContentText(message);
        alert.showAndWait();
    }
    private void alertEchec( SQLException ex){
        Alert alert=new Alert(Alert.AlertType.INFORMATION);
        alert.setResizable(true);
        alert.setWidth(500);
        alert.setTitle("Echec !!!! ");
        alert.setHeaderText("Opération a echoué");
        alert.setContentText("Voici le message envoyé par le serveur\n"+ex.getMessage());
        alert.showAndWait();
    }
    @Override

    public void handle(ActionEvent actionEvent) {
        String nom=((Button)actionEvent.getTarget()).getText();
        Employe e;
        ExerciceJDBC jbd=this.testJDBC.getExerciceJDBC();
        FicheEmploye ficheEmploye=this.testJDBC.getFicheEmploye();
        switch (nom) {
            case "Créer":
                e = ficheEmploye.getEmploye();
                try {
                    jbd.ajouterEmp(e);
                    alertOK("Insertion du joueur a réussi");
                    ficheEmploye.viderFiche();
                } catch (SQLException ex) {
                    alertEchec(ex);
                }
                break;
            } 
        }

}
