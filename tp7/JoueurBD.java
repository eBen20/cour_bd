import java.sql.*;
import java.util.ArrayList;
import java.util.Set;

public class JoueurBD {
	ConnexionMySQL laConnexion;
	Statement st;
	JoueurBD(ConnexionMySQL laConnexion){
		this.laConnexion=laConnexion;
	}

	public int maxNumJoueur() throws SQLException{
		this.st = this.laConnexion.createStatement();
		// Execution de la requete
		ResultSet rs = st.executeQuery("select ifnull(max(numJoueur), 0) leMax from JOUEUR;");
		//Chargement première ligne
		rs.next();
		int res = rs.getInt(1);
		rs.close();
		return res;
		
	}


	int insererJoueur( Joueur j) throws  SQLException{
		//Prépare la requete
		PreparedStatement ps = laConnexion.prepareStatement(
			"insert into JOUEUR(numJoueur,pseudo,motdepasse,sexe,abonne,niveau) values (?, ?, ?, ?, ?, ?)"
			);
		int nouvNum = maxNumJoueur() + 1;
		ps.setInt(1, nouvNum);
		ps.setString(2, j.getPseudo());
		ps.setString(3, j.getMotdepasse());
		ps.setString(4, "" + j.getSexe());
		String abo;
		if (j.isAbonne()){abo = "O";}
		else{abo = "N";}
		ps.setString(5, abo);
		ps.setInt(6, j.getNiveau());
		ps.executeUpdate();
		return nouvNum;
	}


	void effacerJoueur(int num) throws SQLException {
		//Prépare la requete
		PreparedStatement ps = laConnexion.prepareStatement(
			"delete from JOUEUR where numJoueur=?"
			);
		ps.setInt(1, num);
		ps.executeUpdate();
	}

    	void majJoueur(Joueur j)throws SQLException{
			PreparedStatement ps = laConnexion.prepareStatement("update JOUEUR set numJoueur=?, pseudo=?, motdepasse=?, niveau=?, sexe=?, abonne=? where numJoueur=?");
			ps.setInt(1, j.getIdentifiant());
			ps.setString(2,j.getPseudo());
			ps.setString(3, j.getMotdepasse());
			ps.setString(4, " " + j.getSexe());
			String abo;
			if( j.isAbonne()){ abo="O";}
			else{abo ="N";}
			ps.setString(5, abo);
			ps.setInt(6, j.getNiveau());
			ps.setInt(7, j.getIdentifiant());
			int nb=ps.executeUpdate();
			if( nb == 0){
				throw
					new SQLException("suppresion a échoué car aucun joueur ne porte ce num" + j.getIdentifiant());
				
			}
    	}

	Joueur rechercherJoueurParNum(int num)throws SQLException{
		//Execution de la requete
		PreparedStatement ps = laConnexion.prepareStatement("select * from JOUEUR where numJoueur=?");
		//Le paramètre 1 soit "?" sera égal à num
		ps.setInt(1, num);
		ResultSet rs = ps.executeQuery(); //Pas besoin d'argument car ps est PreparedStatement avec la requete
		if (rs.next()){
			// Création d'un joueur pour y mettre le résultat
				//getInt permet de récuperer les information d'une colone à la ligne ou nous somme
			Joueur j = new Joueur(
				rs.getInt("numJoueur"), 
				rs.getString("pseudo"), 
				rs.getString("motdepasse"), 
				rs.getInt("niveau"), 
				rs.getString("sexe").charAt(0), 
				rs.getString("abonne").charAt(0) == '0');
				return j;
		}
		else{
			throw new SQLException("Joueur non trouvé");
		}
	}

	ArrayList<Joueur> listeDesJoueurs() throws SQLException{
		ArrayList<Joueur> resultat = new ArrayList<Joueur>();
		this.st=laConnexion.createStatement();
		ResultSet rs=st.executeQuery("select * from JOUEUR");
		while (rs.next()){
			Joueur j = new Joueur(rs.getInt("numJoueur"), 
				rs.getString("pseudo"), 
				rs.getString("motdepasse"), 
				rs.getInt("niveau"), 
				rs.getString("sexe").charAt(0), 
				rs.getString("abonne").charAt(0) == '0');
			resultat.add(j);
		}
		rs.close();
		return resultat;
	}
	
	String rapportMessage() throws SQLException{
		this.st = laConnexion.createStatement();
		ResultSet rs = st.executeQuery(
			"select pseudo, date(dateMsg) ladate, count(*) nbmsg" + 
			"from MESSAGE, JOUEUR"+
			"where JOUEUR.numJoueur=idUt1 "+
			"group by pseudo, date(dateMsg)"
		);
		String res=" ";
		while( rs.next()){
			res+=rs.getString("pseudo")+" "+rs.getDate("ladate")+" "+rs.getInt("nbmsg")+'\n';
		}
		rs.close();
		return res;


	}
	
	String rapportMessageComplet() throws SQLException{
		this.st = laConnexion.createStatement();
		ResultSet rs = st.executeQuery(
			"select j2.pseudo pseudoRec, dateMsg, j1.pseudo pseudoExp, contenuMsg "+
			"from MESSAGE, JOUEUR j1, JOUEUR j2"+
			"where j1.numJoueur=idUt1 "
		)
   	}
}
